#ifndef SYNC_SERIAL_PORT_HPP_
#define SYNC_SERIAL_PORT_HPP_

#include <string>
#include <mutex>
#include <queue>
#include <termios.h>

class SyncSerialPort
{
private:
    std::recursive_mutex _mutex;
    int fd;

public:
    SyncSerialPort(const std::string file, speed_t baud_rate);

    ~SyncSerialPort();

    bool writeBinary(std::vector<std::uint8_t> data);
    bool readBinary(std::vector<std::uint8_t>& readData, std::size_t size, unsigned int timeout_ms);

    bool writeString(const std::string stringData, bool nullTerminate = true);

    void flush();

    void lockPort();
    void unlockPort();

    std::unique_lock<std::recursive_mutex> getScopedLock();
};

#endif
