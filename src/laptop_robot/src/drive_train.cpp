#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <SyncSerialPort.hpp>
#include <laptop_robot/WheelDeltas.h>

void velocityCmdCallback(const geometry_msgs::Twist& cmd);
double ticksToDist(int ticks);
int distToTicks(double dist);
void setWheelVelocities(double left, double right);
bool getWheelDeltas(double& left, double& right);
bool serviceCallback(laptop_robot::WheelDeltasRequest&  req,
                     laptop_robot::WheelDeltasResponse& res);

SyncSerialPort* port;

int main(int argc, char** argv)
{
    SyncSerialPort portI("/dev/ttyACM0", 115200);
    port = &portI;

    ros::init(argc, argv, "drive_train");
	ros::NodeHandle nh;

    ros::Subscriber cmdSub = nh.subscribe("cmd_vel", 1000, velocityCmdCallback);
    ros::ServiceServer service = nh.advertiseService("get_wheel_deltas", serviceCallback);

    ros::spin();

    setWheelVelocities(0, 0);
	return 0;
}

bool serviceCallback(laptop_robot::WheelDeltasRequest&  req,
                     laptop_robot::WheelDeltasResponse& res)
{
    double leftDelta, rightDelta;
    const bool success = getWheelDeltas(leftDelta, rightDelta);

    res.LeftDelta = leftDelta;
    res.RightDelta = rightDelta;

    return success;
}

void velocityCmdCallback(const geometry_msgs::Twist& cmd)
{
    static constexpr double driveTrainRadius = 0.362 / 2.0;

    float leftVelocity = cmd.linear.x - cmd.angular.z * driveTrainRadius;
    float rightVelocity = cmd.linear.x + cmd.angular.z * driveTrainRadius;

    setWheelVelocities(leftVelocity, rightVelocity);
}

double ticksToDist(int ticks)
{
    static constexpr double wheelRadius = 0.0381;
    return (ticks / 720.0) * wheelRadius * 2.0 * M_PI;
}

int distToTicks(double dist)
{
    static constexpr double wheelRadius = 0.0381;

    return std::round((dist / (wheelRadius * 2.0 * M_PI)) * 720.0);
}

void setWheelVelocities(double left, double right)
{
    static constexpr unsigned char SetCommand = 0x01;

    std::int16_t leftTicks = distToTicks(left);
    std::int16_t rightTicks = distToTicks(right);

    port->writeBinary({ SetCommand, leftTicks >> 8, leftTicks, rightTicks >> 8, rightTicks });
}

bool getWheelDeltas(double& left, double& right)
{
    static constexpr unsigned char GetCommand = 0x00 ;

    static int lastLeftWheelTicks = 0;
    static int lastRightWheelTicks = 0;
    static bool firstRead = true;

    port->writeBinary({ 0x00 });

    std::vector<unsigned char> data;
    if (!port->readBinary(data, 8, 40))
        return false;

    std::int32_t leftPulses, rightPulses;
    leftPulses = (data[0] << 24) | (data[1] << 16) | (data[2] << 8) | (data[3] << 0);
    rightPulses = (data[4] << 24) | (data[5] << 16) | (data[6] << 8) | (data[7] << 0);

    if (!firstRead)
    {
        left = ticksToDist(leftPulses - lastLeftWheelTicks);
        right = ticksToDist(rightPulses - lastRightWheelTicks);
    }
    else
    {
        left = 0;
        right = 0;
        firstRead = false;
    }

    lastLeftWheelTicks = leftPulses;
    lastRightWheelTicks = rightPulses;

    return true;
}