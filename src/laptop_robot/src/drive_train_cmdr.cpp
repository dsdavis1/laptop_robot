//
// Created by spencer on 7/26/17.
//

#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <laptop_robot/WheelDeltas.h>

int main(int argc, char** argv)
{
    ros::init(argc, argv, "drive_train_cmdr");
    ros::NodeHandle nh;

    ros::Publisher cmd = nh.advertise<geometry_msgs::Twist>("cmd_vel", 1000);
    ros::ServiceClient client = nh.serviceClient<laptop_robot::WheelDeltas>("get_wheel_deltas");

    ros::Rate rate(1);

    while (ros::ok())
    {
        geometry_msgs::Twist twist;
        twist.linear.x = 0.5;
        //twist.angular.z = 100.0 * 0.0174533;
        cmd.publish(twist);

        laptop_robot::WheelDeltas deltas;
        if (client.call(deltas))
        {
            ROS_INFO_STREAM("Left Deltas: " << deltas.response.LeftDelta <<
                            " Right Deltas: " << deltas.response.RightDelta);
        }

        ros::spinOnce();
        rate.sleep();
    }

    return 0;
}